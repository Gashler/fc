# About

This is the front-end app for FunderCat, built with Angular.

# Dependencies

- NodeJS
- NPM
- Yarn (optional)
- Angular-CLI

Take a look at `package.json` to see what the latest versions being used are.

# Installation

Clone the repository:
```bash
git clone git@gitlab.com:Gashler/fc.git
```

Go to the newly-cloned directory, check out the develop branch, and install the dependencies with `yarn`/`npm`
```bash
cd fc
git checkout develop
yarn install
```

Copy the staging environment file to a new local environment file:
```bash
cp src/environments/environment.stag.ts src/environments/environment.ts
```

Open the new `environment.ts` file in an editor and update the variables for your local environment. Example:
```bash
export const environment: Environment = {
    production: false,
    api: 'http://api.fc.local', // update this
    seo_url: 'http://api.fc.local', // update this
    app_name: 'FunderCat',
    app_url: 'http://fc.local', // update this
    stripe_key: 'pk_test_SSaPmUdDHaE8XkXt3IkeM4D0',
};
```

# Running the Development Server

Start the `node.js` server with `yarn`/`npm` (which transpiles via webpack):
```bash
yarn start
```

If you haven't already, you'll need to set up the `fc-backend` project before this project will work.

Navigate to `http://localhost:4200/`. The browser should automatically reload if you change any of the source files. If the browser doesn't automatically reload, try running:
```bash
echo 65536 | sudo tee -a /proc/sys/fs/inotify/max_user_watches
```

# Code scaffolding

To generate a new component, run:
```bash
ng generate component component-name
```

You can also use:
```bash
ng generate directive|pipe|service|class|guard|interface|enum|module
```


# Building the Project

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.


## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).


## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).


## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
