// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

import { Environment } from '../app/models/environment.model';

export const environment: Environment = {
    production: false,
    api: 'https://dev.api.fundercat.com',
    seo_url: 'https://dev.my.fundercat.com',
    app_name: 'FunderCat',
    app_url: 'https://dev.fundercat.com',
    stripe_key: 'pk_test_SSaPmUdDHaE8XkXt3IkeM4D0',
};
