import { Component, Inject, Injectable, OnInit } from '@angular/core';
import { LoginService } from '../services/login.service';
import { NotificationsService } from 'angular2-notifications';
import { Observable } from 'rxjs/Observable';
import { TokenResponse } from '../models/token.model';
import { TokenService } from '../services/token.service';

import { FC_ENV, Environment} from '../models';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
    templateUrl: './methods.component.html',
})

@Injectable()
export class MethodsComponent implements OnInit {

    private id: number;
    private method: string;

    constructor(
        @Inject(FC_ENV) protected env: Environment,
        protected loginService: LoginService,
        protected notify: NotificationsService,
        private route: ActivatedRoute,
        private router: Router,
        protected tokenService: TokenService
    ) {}

    public ngOnInit(): void {
        this.id = this.route.snapshot.params.id;
        this[this.route.snapshot.params.method]();
    }

    /**
     * Allow admins to login as a a given user.
     */
    public loginAsUser(): void {
        let response = this.loginService
            .loginAsUser(this.id)
            .subscribe(
                (response: TokenResponse) => {
                    this.tokenService.store(response);
                    this.router.navigate([`/`]);
                    window.location.reload();
                },
                (error: any) => {
                    this.notify.error(error.message);
                }
            );
        this.tokenService.store(response);
    }
}
