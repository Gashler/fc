import { PaginatedResponse } from './paginated-response.model';
import { Subscription } from './subscription.model';

export class User {
    // constructor(user: User) {
    //     Object.assign(this, user);
    // }
    public id: number;
    public created_at: Date;
    public has_password?: boolean;
    public updated_at: Date;
    public first_name: string;
    public last_name: string;
    public name: string;
    public email: string;
    public password?: string;
    public c_password?: string;
    public role_id: number;
    public stripe_account?: any[];
    public subscriptions?: Subscription[];
    public username: string;
    public role: string;
}


export class UserPaginatedResponse extends PaginatedResponse {
    public data: User[];
}
