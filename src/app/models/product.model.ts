import {PaginatedResponse} from './paginated-response.model';
import {User} from './user.model';
import {Merchant} from './merchant.model';
import {Vote} from './vote.model';
import {Image} from './image.model';

export class Product {
  public accepted: boolean;
  public approved: boolean;
  public created_at: Date;
  public description: string;
  public discounted_price: number;
  public featured_image: Image;
  public id: number;
  public images: Image[];
  public isVoted: boolean;
  public merchant: Merchant;
  public merchant_id: number;
  public name: string;
  public price: number;
  public regular_price: number;
  public short_description: string;
  public type: string;
  public updated_at: Date;
  public user: User;
  public user_id: number;
  public vote_count: number;
  public votes: Vote[];


  /**
   * Check if product has vote by user id.
   * @param product Product
   * @param id number - user id
   */
  public static findVote(product: Product, user_id: number): Vote {
    return product.votes.find(vote => vote.user_id == user_id);
  }

  /**
   * Add vote to user votes
   * @param product Product
   * @param vote Vote - vote to add
   */
  public static addVote(product: Product, vote: Vote): void {
    product.vote_count++;
    product.isVoted = true;
    product.votes.push(vote);
  }

  /**
   * Add vote to user votes
   * @param product Product
   * @param vote Vote - vote to add
   */
  public static removeVote(product: Product, vote: Vote): void {
    product.vote_count--;
    product.isVoted = false;
    product.votes = product.votes.filter(v => v.id !== vote.id);
  }
}

export class ProductPaginatedResponse extends PaginatedResponse {
  public data: Product[];
}
