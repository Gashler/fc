import { PaginatedResponse } from './paginated-response.model';
import { Campaign } from './campaign.model';
import { User } from './user.model';

export class Subscription {
  public id: number;
  public campaign?: Campaign;
  public campaign_id?: number;
  public created_at: Date;
  public disabled: boolean;
  public end_at: Date;
  public key: string;
  public name: string;
  public start_at: Date;
  public updated_at: Date;
  public users?: User[];
}

export class SubscriptionPaginatedResponse extends PaginatedResponse {
  public data: Subscription[];
}
