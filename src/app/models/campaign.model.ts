import { PaginatedResponse } from './paginated-response.model';
import { Comment } from './comment.model';
import { Image } from './image.model';
import { Link } from './link.model';
import { Reward } from './reward.model';
import { Payment } from './payment.model';
import { Subscription } from './subscription.model';
import { User } from './user.model';

export class Campaign {
    constructor(params?) {
        Object.assign(this, params);
    }
    public id: number;
    public backers_count: number;
    public created_at: Date;
    public comments: Comment[];
    public days: number;
    public long_description: string;
    public image?: Image;
    public image_id?: number;
    public images: Image[];
    public links: Link[];
    public live: boolean;
    public monetary_goal: number;
    public monetary_total: number;
    public payments?: Payment[];
    public percent_raised: number;
    public rewards?: Reward[];
    public short_description: string;
    public subscription?: Subscription;
    public title: string;
    public updated_at: Date;
    public user: User;
    public user_id: number;
    public users?: User[];
    public safe_url?: string;
    public video_id?: number;
    public video?: Link;
}


export class CampaignPaginatedResponse extends PaginatedResponse {
    public data: Campaign[];
}
