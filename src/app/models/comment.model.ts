import { PaginatedResponse } from './paginated-response.model';
import { User } from './user.model';

export class Comment {
  public id: number;
  public approved: boolean;
  public commentable_type: string;
  public commentable_id: number;
  public body: string;
  public updated_at: Date;
  public user: User;
  public user_id: number;
}

export class CommentPaginatedResponse extends PaginatedResponse {
  public data: Comment[];
}
