import { PaginatedResponse } from './paginated-response.model';

import { Campaign } from './campaign.model';
import { User } from './user.model';

export class Payment {
  public id: number;
  public anonymous?: boolean;
  public campaign?: Campaign;
  public campaign_id: number;
  public created_at: Date;
  public amount: number;
  public card_brand: string;
  public card_last4: number;
  public updated_at: Date;
  public reward_id?: number;
  public token?: string;
  public user?: User;
  public user_id: number;
}


export class PaymentPaginatedResponse extends PaginatedResponse {
  public data: Payment[];
}
