import { PaginatedResponse } from './paginated-response.model';

export class Link {
  public id: number;
  public created_at: Date;
  public embed_url?: string;
  public key: string;
  public linkable_id: number;
  public linkable_type: string;
  public name?: string;
  public placeholder?: string;
  public updated_at: Date;
  public value: string;
  public safe_url?: any;
}


export class CampaignPaginatedResponse extends PaginatedResponse {
  public data: Link[];
}
