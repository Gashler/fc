// PaginatedResponse is to be extended
export class PaginatedResponse {
  public current_page: number;
  public last_page: number;
  public per_page: number;
  public from: number;
  public to: number;
  public total: number;
  public path: string;
  public first_page_url: string;
  public last_page_url: string;
  public next_page_url: string;
  public prev_page_url: string;

  // Extenders should define the data type
  // public data: Object|Array<any>;
}
