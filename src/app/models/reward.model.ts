import { PaginatedResponse } from './paginated-response.model';

export class Reward {
    public amount: number;
    public created_at: Date;
    public description: string;
    public id: number;
    public title: string;
    public updated_at: Date;
}


export class RewardPaginatedResponse extends PaginatedResponse {
  public data: Reward[];
}
