import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Import Components
import { LayoutComponent } from './layout';
import { LoginComponent } from './views/login';
import { NotFoundComponent } from './views/not-found';

// Guards
import { AuthGuard } from './guards/auth-guard.service';


// Note: Add dashboard routes to the DashboardRoutingModule
export const routes: Routes = [
  {
    path: 'login',
    component: LoginComponent,
  },
  {
    path: '',
    component: LayoutComponent,
    children: [
      {
        path: '',
        loadChildren: './views/dashboard/dashboard.module#DashboardModule'
      },
    ]
  },

  // 404
  {
    path: '**',
    component: NotFoundComponent,
  },
];


@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
