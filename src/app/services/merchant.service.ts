import { Injectable, Inject } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import * as _ from 'lodash';

import { HttpService } from './http.service';
import { Merchant, MerchantPaginatedResponse } from '../models/merchant.model';

import { FC_ENV, Environment } from '../models';


@Injectable()
export class MerchantService {
  public omit: Array<string> = ['created_at', 'updated_at'];

  constructor(protected http: HttpService,
              @Inject(FC_ENV) protected environment: Environment) {
  }

  /**
   * Request the merchants.
   */
  public all(): Observable<MerchantPaginatedResponse> {
    return this.http
      .hostname(this.environment.api)
      .get('merchants');
  }

  /**
   * Request a given merchant.
   */
  public one(id: number): Observable<Merchant> {
    return this.http
      .hostname(this.environment.api)
      .get(`merchants/${id}?with=google_places`);
  }

  /**
   * Store the given merchant.
   * Figures out if we should POST or PATCH whether there is an ID.
   */
  public store(merchant: Merchant): Observable<Merchant> {
    if (merchant.id > 0) {
      return this.patch(_.omit(merchant, this.omit));
    }

    return this.post(merchant);
  }

  /**
   * Create a merchant.
   */
  public post(merchant: Merchant): Observable<Merchant> {
    return this.http
      .hostname(this.environment.api)
      .post('merchants', merchant);
  }

  /**
   * Update a merchant.
   */
  public patch(merchant: Merchant): Observable<Merchant> {
    return this.http
      .hostname(this.environment.api)
      .patch(`merchants/${merchant.id}`, merchant);
  }

  /**
   * Delete a merchant.
   */
  public delete(merchant: Merchant): Observable<null> {
    return this.http
      .hostname(this.environment.api)
      .delete(`merchants/${merchant.id}`);
  }
}
