import { Injectable, Inject } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import * as _ from 'lodash';
import { HttpService } from './http.service';
import { Comment, CommentPaginatedResponse } from '../models/comment.model';
import { FC_ENV, Environment } from '../models';


@Injectable()
export class CommentService {
  public omit: Array<string> = ['created_at', 'updated_at', 'name'];

  constructor(protected http: HttpService,
              @Inject(FC_ENV) protected environment: Environment) {
  }

  /**
   * Request the comments.
   */
  public all(): Observable<CommentPaginatedResponse> {
    return this.http
      .hostname(this.environment.api)
      .get('comments');
  }

  /**
   * Request data for comment form
   */
  public create(campaign_id: number, reward_id: number): Observable<any> {
    return this.http
      .hostname(this.environment.api)
      .get(`comments/create/${campaign_id}/${reward_id}`);
  }

  /**
   * Request a given comment.
   */
  public one(id: number): Observable<Comment> {
    return this.http
      .hostname(this.environment.api)
      .get(`comments/${id}?with=campaign,campaign.subscription,reward`);
  }

  /**
   * Store the given comment.
   * Figures out if we should POST or PATCH whether there is an ID.
   */
  public store(comment: Comment): Observable<Comment> {
    if (comment.id > 0) {
      return this.patch(_.omit(comment, this.omit));
    }

    return this.post(comment);
  }

  /**
   * Create a comment.
   */
  public post(comment: Comment): Observable<Comment> {
      console.log('CommentService:60!');
    return this.http
      .hostname(this.environment.api)
      .post('comments', comment);
  }

  /**
   * Update a comment.
   */
  public patch(comment: Comment): Observable<Comment> {
    return this.http
      .hostname(this.environment.api)
      .patch(`comments/${comment.id}`, comment);
  }

  /**
   * Delete a comment.
   */
  public delete(comment: Comment): Observable<null> {
    return this.http
      .hostname(this.environment.api)
      .delete(`comments/${comment.id}`);
  }
}
