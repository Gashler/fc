import { Injectable, Inject } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import * as _ from 'lodash';
import { HttpService } from './http.service';
import { Payment, PaymentPaginatedResponse } from '../models/payment.model';
import { FC_ENV, Environment } from '../models';

@Injectable()
export class PaymentService {
  public omit: Array<string> = ['created_at', 'updated_at', 'name'];

  constructor(
      protected http: HttpService,
      @Inject(FC_ENV) protected environment: Environment
  ) {}

  /**
   * Request the payments.
   */
  public all(): Observable<PaymentPaginatedResponse> {
    return this.http
      .hostname(this.environment.api)
      .get('payments');
  }

  /**
   * Request a given payment.
   */
  public one(id: number): Observable<Payment> {
    return this.http
      .hostname(this.environment.api)
      .get(`payments/${id}?with=campaign,campaign.subscription,campaign.user,reward`);
  }

  /**
   * Store the given payment.
   * Figures out if we should POST or PATCH whether there is an ID.
   */
  public store(payment: Payment, endpoint?: string): Observable<Payment> {
    if (payment.id > 0) {
      return this.patch(_.omit(payment, this.omit));
    }

    return this.post(payment, endpoint);
  }

  /**
   * Create a payment.
   */
  public post(payment: Payment, endpoint?: string): Observable<Payment> {
      if (!endpoint) {
          endpoint = 'payments';
      }
    return this.http
      .hostname(this.environment.api)
      .post(endpoint, payment);
  }

  /**
   * Update a payment.
   */
  public patch(payment: Payment): Observable<Payment> {
    return this.http
      .hostname(this.environment.api)
      .patch(`payments/${payment.id}`, payment);
  }

  /**
   * Delete a payment.
   */
  public delete(payment: Payment): Observable<null> {
    return this.http
      .hostname(this.environment.api)
      .delete(`payments/${payment.id}`);
  }
}
