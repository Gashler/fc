import { Injectable, Inject } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import * as _ from 'lodash';

import { HttpService } from './http.service';
import { Product, ProductPaginatedResponse } from '../models';

import { FC_ENV, Environment } from '../models';


@Injectable()
export class ProductService {
  public omit: Array<string> = [
      'categories',
      'created_at',
      'featured_image',
      'google_places',
      'merchant',
      'price',
      'updated_at',
      'user',
      'vote_count',
      'votes'
  ];

  constructor(protected http: HttpService,
              @Inject(FC_ENV) protected environment: Environment) {
  }

  /**
   * Request the products.
   */
  public all(): Observable<ProductPaginatedResponse> {
    return this.http
      .hostname(this.environment.api)
      .get('products?with=user,merchant');
  }

  /**
   * Request the products with votes
   */
  public allWithVotes(): Observable<ProductPaginatedResponse> {
    return this.http
      .hostname(this.environment.api)
      .get('products?with=user,merchant,votes&where=approved~1');
  }

  /**
   * Request a given product.
   */
  public one(id: number): Observable<Product> {
    return this.http
      .hostname(this.environment.api)
      .get(`products/${id}?with=user,merchant,images,votes,votes.user,google_places`);
  }

  /**
   * Store the given user.
   * Figures out if we should POST or PATCH whether there is an ID.
   */
  public store(product: Product): Observable<Product> {
    if (product.id > 0) {
      return this.patch(product);
    }

    return this.post(product);
  }

  /**
   * Create a product.
   */
  public post(product: Product): Observable<Product> {
    return this.http
      .hostname(this.environment.api)
      .post('products', product);
  }

  /**
   * Update a product.
   */
  public patch(product: Product): Observable<Product> {
    return this.http
      .hostname(this.environment.api)
      .patch(`products/${product.id}`, _.omit(product, this.omit));
  }

  /**
   * Delete a product.
   */
  public delete(product: Product): Observable<null> {
    return this.http
      .hostname(this.environment.api)
      .delete(`products/${product.id}`);
  }
}
