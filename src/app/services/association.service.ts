import { Injectable, Inject } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { HttpService } from './http.service';

import { FC_ENV, Environment } from '../models';


@Injectable()
export class AssociationService {
  constructor(protected http: HttpService,
              @Inject(FC_ENV) protected environment: Environment) {
  }

  /**
   * Associate a subresource such as 'images' with a resource such as 'products'.
   */
  public post(resource: string, resourceId: number, subResource: string, subResourceId: number): Observable<null> {
      console.log('resource = ', resource);
    return this.http
      .hostname(this.environment.api)
      .post(`${resource}/${resourceId}/${subResource}/${subResourceId}`, {});
  }

  /**
   * Remove a subresource association such as 'images' from a resource such as 'products'.
   */
  public delete(resource: string, resourceId: number, subResource: string, subResourceId: number): Observable<null> {
    return this.http
      .hostname(this.environment.api)
      .delete(`${resource}/${resourceId}/${subResource}/${subResourceId}`);
  }
}
