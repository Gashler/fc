import {Injectable, Inject} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import * as _ from 'lodash';

import {HttpService} from './http.service';

import { FC_ENV, Environment } from '../models';

import {Vote} from './../models/vote.model';
import {PaginatedResponse} from './../models/paginated-response.model'

@Injectable()
export class VoteService {
  public omit: Array<string> = ['created_at', 'updated_at'];

  constructor(protected http: HttpService,
              @Inject(FC_ENV) protected environment: Environment) {
  }

  /**
   * Request the votes.
   */
  public all(): Observable<VotePaginatedResponse> {
    return this.http
      .hostname(this.environment.api)
      .get('votes');
  }

  /**
   * Request a given vote.
   */
  public one(id: number): Observable<Vote> {
    return this.http
      .hostname(this.environment.api)
      .get(`votes/${id}`);
  }

  /**
   * Store the given vote.
   * Figures out if we should POST or PATCH whether there is an ID.
   */
  public store(vote: Vote): Observable<Vote> {
    if (vote.id > 0) {
      return this.patch(_.omit(vote, this.omit));
    }

    return this.post(vote);
  }

  /**
   * Create a vote.
   */
  public post(vote: Vote): Observable<Vote> {
    return this.http
      .hostname(this.environment.api)
      .post('votes', vote);
  }

  /**
   * Update a vote.
   */
  public patch(vote: Vote): Observable<Vote> {
    return this.http
      .hostname(this.environment.api)
      .patch(`votes/${vote.id}`, vote);
  }

  /**
   * Delete a vote.
   */
  public delete(vote: Vote): Observable<null> {
    return this.http
      .hostname(this.environment.api)
      .delete(`votes/${vote.id}`);
  }
}

export class VotePaginatedResponse extends PaginatedResponse {
  public data: Vote[];
}
