import { Injectable, Inject } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import * as _ from 'lodash';

import { HttpService } from './http.service';
import { Image, ImagePaginatedResponse } from '../models/image.model';

import { FC_ENV, Environment } from '../models';


@Injectable()
export class ImageService {
  public omit: Array<string> = ['created_at', 'updated_at'];

  constructor(protected http: HttpService,
              @Inject(FC_ENV) protected environment: Environment) {
  }

  /**
   * Request the images.
   */
  public all(): Observable<ImagePaginatedResponse> {
    return this.http
      .hostname(this.environment.api)
      .get('images');
  }

  /**
   * Request a given image.
   */
  public one(id: number): Observable<Image> {
    return this.http
      .hostname(this.environment.api)
      .get(`images/${id}`);
  }

  /**
   * Store the given image.
   * Figures out if we should POST or PATCH whether there is an ID.
   */
  public store(image: Image): Observable<Image> {
    if (image.id > 0) {
      return this.patch(_.omit(image, this.omit));
    }

    return this.post(image);
  }

  /**
   * Create a image.
   */
  public post(image: Image): Observable<Image> {
    return this.http
      .hostname(this.environment.api)
      .post('images', image);
  }

  /**
   * Update a image.
   */
  public patch(image: Image): Observable<Image> {
    return this.http
      .hostname(this.environment.api)
      .patch(`images/${image.id}`, image);
  }

  /**
   * Delete a image.
   */
  public delete(image: Image): Observable<null> {
    return this.http
      .hostname(this.environment.api)
      .delete(`images/${image.id}`);
  }
}
