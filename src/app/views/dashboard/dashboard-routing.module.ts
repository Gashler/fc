import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AboutComponent } from '../company/about';
import { ContactComponent } from '../company/contact';
import { CampaignCreateComponent } from '../campaigns/create';
import { CampaignDashboardComponent } from '../campaigns/dashboard';
import { CampaignIndexComponent } from '../campaigns/index';
import { CampaignShowComponent } from '../campaigns/show';
import { HomeComponent } from '../home';
import { MethodsComponent } from '../../methods/methods.component';
import { PaymentCreateComponent } from '../payments/create';
import { PaymentConfirmationComponent } from '../payments/confirmation';
import { UserRegisterComponent } from '../users/register';
import { UserIndexComponent } from '../users/index';
import { UserEditComponent } from '../users/edit';
import { UserFinancialComponent } from '../users/financial';

// Guards
import { AuthGuard } from '../../guards/auth-guard.service';
import { RoleGuard } from '../../guards/role-guard.service';

const routes: Routes = [
    {
        path: 'about',
        component: AboutComponent,
    },
    {
        path: 'contact',
        component: ContactComponent,
    },
    {
        path: '',
        component: HomeComponent,
    },
    {
        path: 'campaigns',
        component: CampaignIndexComponent
    },
    {
        canActivate: [RoleGuard],
        canLoad: [RoleGuard],
        path: 'campaigns/new',
        component: CampaignCreateComponent,
        data: {
            redirect: 'register'
        }
    },
    {
        canActivate: [AuthGuard],
        canLoad: [AuthGuard],
        path: 'campaigns/:id/dashboard',
        component: CampaignDashboardComponent,
    },
    {
        canActivate: [AuthGuard],
        canLoad: [AuthGuard],
        path: 'campaigns/:id/dashboard/:sectionIndex',
        component: CampaignDashboardComponent,
    },
    // {
    //     canActivate: [AuthGuard],
    //     canLoad: [AuthGuard],
    //     path: 'campaigns/{:id}/dashboard/{:section}',
    //     component: CampaignDashboardComponent,
    // },
    {
        canActivate: [RoleGuard],
        canLoad: [RoleGuard],
        path: 'campaigns/new/:register',
        component: CampaignCreateComponent,
        data: {
            redirect: 'register'
        }
    },
    {
        canActivate: [AuthGuard],
        canLoad: [AuthGuard],
        path: 'campaigns/:id/:method',
        component: CampaignCreateComponent,
    },
    {
        canActivate: [AuthGuard],
        canLoad: [AuthGuard],
        path: 'campaigns/:id/:method/:step',
        component: CampaignCreateComponent,
    },
    {
        path: 'campaigns/:id',
        component: CampaignShowComponent,
    },
    {
        path: 'methods/:method/:id',
        component: MethodsComponent,
    },
    {
        path: 'payments/:id/confirmation',
        component: PaymentConfirmationComponent,
    },
    {
        path: 'payments/:id/confirmation/:step',
        component: PaymentConfirmationComponent,
    },
    {
        path: 'payments/new/:campaign_id',
        component: PaymentCreateComponent,
    },
    {
        path: 'payments/new/:campaign_id/:reward_index',
        component: PaymentCreateComponent,
    },
    {
        canActivate: [RoleGuard],
        canLoad: [RoleGuard],
        path: 'users',
        component: UserIndexComponent,
        data: {
            role: 'admin'
        }
    },
    {
        path: 'register',
        component: UserRegisterComponent
    },
    {
        canActivate: [AuthGuard],
        canLoad: [AuthGuard],
        path: 'user/edit',
        component: UserEditComponent,
    },
    {
        canActivate: [AuthGuard],
        canLoad: [AuthGuard],
        path: 'user/financial',
        component: UserFinancialComponent,
    },
    {
        canActivate: [AuthGuard],
        canLoad: [AuthGuard],
        path: 'user/financial/:response',
        component: UserFinancialComponent,
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class DashboardRoutingModule { }
