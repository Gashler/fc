import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { FC_ENV } from '../../models/environment.model';
import { environment } from '../../../environments/environment';

// Services
import { HttpService } from '../../services/http.service';
import { CampaignService } from '../../services/campaign.service';
import { CommentService } from '../../services/comment.service';
import { PaymentService } from '../../services/payment.service';
import { UserService } from '../../services/user.service';
import { ImageService } from '../../services/image.service';
import { AssociationService } from '../../services/association.service';

// Components
import { AboutComponent } from '../company/about/about.component';
import { AdminDashboardComponent } from '../dashboard/admin/adminDashboard.component';
import { ContactComponent } from '../company/contact/contact.component';
import { DashboardRoutingModule } from './dashboard-routing.module';
import { CampaignIndexComponent } from '../campaigns/index/campaignIndex.component';
import { CampaignCreateComponent } from '../campaigns/create/campaignCreate.component';
import { CampaignDashboardComponent } from '../campaigns/dashboard/campaignDashboard.component';
import { CampaignShowComponent } from '../campaigns/show/campaignShow.component';
import { HomeComponent } from '../home/home.component';
import { MethodsComponent } from '../../methods/methods.component';
import { PaymentConfirmationComponent } from '../payments/confirmation/paymentConfirmation.component';
import { PaymentCreateComponent } from '../payments/create/paymentCreate.component';
import { UserEditComponent } from '../users/edit/userEdit.component';
import { UserFinancialComponent } from '../users/financial/userFinancial.component';
import { UserIndexComponent } from '../users/index/userIndex.component';
import { UserRegisterComponent } from '../users/register/userRegister.component';

import { EditorModule } from '@tinymce/tinymce-angular';


@NgModule({
    imports: [
        CommonModule,
        EditorModule,
        FormsModule,
        HttpModule,
        DashboardRoutingModule,
    ],
    declarations: [
        AboutComponent,
        AdminDashboardComponent,
        ContactComponent,
        MethodsComponent,
        UserEditComponent,
        UserFinancialComponent,
        UserIndexComponent,
        UserRegisterComponent,
        CampaignCreateComponent,
        CampaignDashboardComponent,
        CampaignIndexComponent,
        CampaignShowComponent,
        HomeComponent,
        PaymentConfirmationComponent,
        PaymentCreateComponent,
    ],
    providers: [
        { provide: FC_ENV, useValue: environment },
        {
            provide: CampaignService,
            useClass: CampaignService,
            deps: [HttpService, FC_ENV],
        },
        {
            provide: CommentService,
            useClass: CommentService,
            deps: [HttpService, FC_ENV],
        },
        {
            provide: PaymentService,
            useClass: PaymentService,
            deps: [HttpService, FC_ENV],
        },
        {
            provide: UserService,
            useClass: UserService,
            deps: [HttpService, FC_ENV],
        },
        {
            provide: ImageService,
            useClass: ImageService,
            deps: [HttpService, FC_ENV],
        },
        {
            provide: AssociationService,
            useClass: AssociationService,
            deps: [HttpService, FC_ENV],
        },
    ],
})
export class DashboardModule { }
