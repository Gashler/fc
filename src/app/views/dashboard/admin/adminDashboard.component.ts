import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { NotificationsService } from 'angular2-notifications';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/empty';

import { CampaignService } from '../../../services/campaign.service';
import { Campaign } from '../../../models/campaign.model';
import { Payment } from '../../../models/payment.model';
import { Reward } from '../../../models/reward.model';

@Component({
    templateUrl: './adminDashboard.component.html',
})
export class AdminDashboardComponent implements OnInit {

    public campaign: Campaign;
    public sections: any = [
        {
            'name': 'Statistics',
            'icon': 'chart-bar'
        },
        {
            'name': 'Payments',
            'icon': 'dollar'
        }
    ];
    public sectionIndex: number = 0;

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private location: Location,
        private notify: NotificationsService,
        private campaignService: CampaignService,
    ) { }

    public ngOnInit(): void {
        // this.section = this.sections[0];
        this.load();
        if (this.route.snapshot.params.section) {
            this.sectionIndex = this.route.snapshot.params.sectionIndex;
        }
    }

    /**
    * Get the campaign.
    */
    public load(): void {
        this.route.params
        .switchMap((params: Params) => {
            let relationships = 'comments,comments.user,payments,payments.reward,payments.user,users';
            return this.campaignService.one(params.id, relationships);
        })
        .subscribe(
            (response: Campaign) => {
                this.campaign = response;
            },
            (error: any) => {
                this.notify.error('Could not load this campaign.');
            },
        );
    }

    /**
    * Change section
    */
    public changeSection(index: number): void {
        this.sectionIndex = index;
        this.router.navigate([`/campaigns/${this.campaign.id}/dashboard/${index}`]);
        console.log(this.sections[this.sectionIndex]);
    }
}
