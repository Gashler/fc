import { Component, Inject } from '@angular/core';
import { Environment, FC_ENV } from '../../../models/environment.model';

@Component({
  templateUrl: './contact.component.html',
})

export class ContactComponent {
    constructor(
        @Inject(FC_ENV) public env: Environment,
    ) {}
}
