import { Component, Inject } from '@angular/core';
import { Environment, FC_ENV } from '../../../models/environment.model';

@Component({
  templateUrl: './about.component.html',
})

export class AboutComponent {
    constructor(
        @Inject(FC_ENV) public env: Environment,
    ) {}
}
