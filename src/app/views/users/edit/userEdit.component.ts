import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { NotificationsService } from 'angular2-notifications';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/empty';

import { TokenService } from '../../../services/token.service';
import { UserService } from '../../../services/user.service';
import { User } from '../../../models/user.model';


@Component({
    selector: 'si-user',
    templateUrl: './userEdit.component.html',
})
export class UserEditComponent implements OnInit {
    public user: User;
    public page = 1;

    constructor(private route: ActivatedRoute,
        private router: Router,
        private location: Location,
        private notify: NotificationsService,
        private tokenService: TokenService,
        private userService: UserService,
    ) {}

    public ngOnInit(): void {
        this.user = this.tokenService.getUser();
        this.load();
    }

    /**
     * Save the user upon submit.
     */
    public onSubmit(): void {
        this.store();
    }

    /**
     * Get the user.
     */
    public load(): void {
        this.userService.one(this.user.id)
            .subscribe(
            (response: User) => {
                this.user = response;
            },
            (error: any) => {
                this.notify.error('Could not load this user.');
            },
        );
    }

    /**
     * Use the UserService to post/patch the user.
     */
    public store(): void {
        // Auto-confirm password from Admin dashboard.
        delete this.user.c_password;
        if (this.user.password !== '') {
            this.user.c_password = this.user.password;
        } else {
            delete this.user.password;
        }

        this.userService
            .store(this.user)
            .subscribe(
            (response: User) => {
                if (response.id > 0) {
                    this.user = response;
                    this.location.go(`/users/${response.id}`);
                }

                this.notify.success('User saved!');
            },
            (error: any) => {
                this.notify.error('Error saving.', error.message || 'Could not save this user.');
            },
        );
    }
}
