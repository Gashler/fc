import { Component, OnInit } from '@angular/core';

import { UserService } from '../../../services/user.service';
import { User, UserPaginatedResponse } from '../../../models/user.model';


@Component({
    selector: 'si-users',
    templateUrl: './userIndex.component.html',
})
export class UserIndexComponent implements OnInit {
    public users: User[];
    public page = 1;

    constructor(private userService: UserService) {
    }

    public ngOnInit(): void {
        this.load();
    }

    /**
     * Get the users.
     */
    public load(page: number = this.page): void {
        this.page = page || 1;

        this.userService
            .all()
            .subscribe((response: UserPaginatedResponse) => {
                this.users = response.data;
            });
    }
}
