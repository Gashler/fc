import { Component, Inject, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { NotificationsService } from 'angular2-notifications';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/empty';

import { TokenService } from '../../../services/token.service';
import { UserService } from '../../../services/user.service';
import { User } from '../../../models/user.model';
import { Environment, FC_ENV } from '../../../models/environment.model';

@Component({
    templateUrl: './userFinancial.component.html',
})
export class UserFinancialComponent implements OnInit {
    public user: User;

    constructor(
        @Inject(FC_ENV) public env: Environment,
        private route: ActivatedRoute,
        private router: Router,
        private location: Location,
        private notify: NotificationsService,
        private tokenService: TokenService,
        private userService: UserService,
    ) {}

    public ngOnInit(): void {
        this.user = this.tokenService.getUser();
        this.load();
        let response = null;
        if (response = this.route.snapshot.params.response) {
            if (response == 'success') {
                this.notify.success('Success.', 'You will now be able to receive payments.');
            }
            if (response == 'error') {
                this.notify.success('Success.', 'There was an error connecting your account. Please contact support.');
            }
        }
    }

    /**
     * Get the user.
     */
    public load(): void {
        this.userService.one(this.user.id, 'stripe_account')
            .subscribe(
            (response: User) => {
                this.user = response;
            },
            (error: any) => {
                this.notify.error('Could not load this user.');
            },
        );
    }
}
