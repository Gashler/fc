import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { NotificationsService } from 'angular2-notifications';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/empty';
import { TokenService } from '../../../services/token.service';
import { UserService } from '../../../services/user.service';
import { User } from '../../../models/user.model';


@Component({
    selector: 'si-user',
    templateUrl: './userRegister.component.html',
})
export class UserRegisterComponent implements OnInit {
    public displayStep: number = 1;

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private location: Location,
        private notify: NotificationsService,
        private tokenService: TokenService,
        private userService: UserService,
        public user: User
    ) {}

    public ngOnInit(): void {
        //
    }

    /**
     * Save the user upon submit.
     */
    public onSubmit(): void {
        this.store();
    }

    /**
     * Use the UserService to post/patch the user.
     */
    public store(): void {
        // Auto-confirm password from Admin dashboard.
        delete this.user.c_password;
        if (this.user.password !== '') {
            this.user.c_password = this.user.password;
        } else {
            delete this.user.password;
        }
        this.userService
            .register(this.user)
            .subscribe(
            (response: User) => {
                if (response['user'].id > 0) {
                    this.tokenService.store(response);
                    this.router.navigate(['/campaigns/new/register']);
                }

                this.notify.success('Account created!');
            },
            (error: any) => {
                this.notify.error('Error saving.', error.message || 'Could not save this user.');
            },
        );
    }
}
