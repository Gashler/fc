import { Component, Inject, OnInit } from '@angular/core';

import { CampaignService } from '../../../services/campaign.service';
import { Campaign, CampaignPaginatedResponse } from '../../../models/campaign.model';
import { TokenService } from '../../../services/token.service';
import { User } from '../../../models/user.model';
import { Environment, FC_ENV } from '../../../models/environment.model';


@Component({
    selector: 'si-campaigns',
    templateUrl: './campaignIndex.component.html',
})
export class CampaignIndexComponent implements OnInit {
    public campaigns: Campaign[];
    public page = 1;
    public user: User;

    constructor(
        @Inject(FC_ENV) public env: Environment,
        private campaignService: CampaignService,
        private tokenService: TokenService
    ) {}

    public ngOnInit(): void {
        this.user = this.tokenService.getUser();
        this.load();
    }

    /**
    * Get the campaigns.
    */
    public load(page: number = this.page): void {
        this.page = page || 1;
        if (this.user.role == 'admin') {
            this.campaignService
            .all()
            .subscribe((response: CampaignPaginatedResponse) => {
                this.campaigns = response.data;
            });
        } else {
            this.campaignService
            .mine()
            .subscribe((response: CampaignPaginatedResponse) => {
                this.campaigns = response.data;
            });
        }
    }
}
