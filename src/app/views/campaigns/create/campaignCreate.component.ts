import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Location } from '@angular/common';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { NotificationsService } from 'angular2-notifications';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/empty';

import { ImageService } from '../../../services/image.service';
import { CampaignService } from '../../../services/campaign.service';
import { PaymentService } from '../../../services/payment.service';
import { AssociationService } from '../../../services/association.service';
import { Campaign } from '../../../models/campaign.model';
import { Image } from '../../../models/image.model';
import { Link } from '../../../models/link.model';
import { Payment } from '../../../models/payment.model';
import { Reward } from '../../../models/reward.model';
import { Environment, FC_ENV } from '../../../models/environment.model';
import { User } from '../../../models/user.model';
import { TokenService } from '../../../services/token.service';

@Component({
    selector: 'si-campaign',
    templateUrl: './campaignCreate.component.html',
})
export class CampaignCreateComponent implements OnInit {
    public fundraisingForm: any;
    @ViewChild('fundraisingForm') public form: NgForm;
    public campaign: Campaign = new Campaign({
        days: 30
    });
    public displayStep: number;
    public method: string;
    public payment: Payment = new Payment();
    public page = 1;
    public stepIndex = 0;
    public steps = [
        {
            name: 'Description',
            icon: 'list',
        },
        {
            name: 'Media',
            icon: 'picture',
        },
        {
            name: 'Fundraising Goals',
            icon: 'wallet',
        },
        {
            name: 'Rewards',
            icon: 'present',
        },
    ];
    public linkTypes = [
        {
            key: 'youtube',
            name: 'Youtube Video',
            placeholder: 'https://youtube.com/watch?v=abc123'
        },
        {
            key: 'facebook',
            name: 'Facebook Page',
            placeholder: 'https://facebook.com/MyPage/'
        },
        {
            key: 'globe',
            name: 'Website',
            placeholder: 'https://mywebsite.com'
        },
    ];
    public linkTypeIndex = 0;
    public uploadWidget: any;
    public user: User;

    constructor(
        @Inject(FC_ENV) public env: Environment,
        private route: ActivatedRoute,
        private router: Router,
        private location: Location,
        private notify: NotificationsService,
        private campaignService: CampaignService,
        private paymentService: PaymentService,
        private imageService: ImageService,
        private associationService: AssociationService,
        private tokenService: TokenService
    ) { }

    public ngOnInit(): void {

        // get user
        this.user = this.tokenService.getUser();

        // define method
        if (this.route.snapshot.params.method) {
            this.method = this.route.snapshot.params.method;
        } else {
            this.method = 'create';
        }

        // define step
        if (this.route.snapshot.params.step) {
            this.stepIndex = this.route.snapshot.params.step;
            this.displayStep = this.route.snapshot.params.step;
        } else {
            this.displayStep = 1;
            this.stepIndex = 0;
        }

        // if user just registered
        if (this.route.snapshot.params.register) {
            this.displayStep = 2;
        }

        this.load();
    }

    public changeStep(index: number): void {
        this.stepIndex = index;
        this.router.navigate([`/campaigns/${this.campaign.id}/edit/${index}`]);
    }

    /**
    * Save context of CampaignComponent for .storeImage()
    * Note: Anonymous function must be defined and used here so we maintain the
    * context of CampaignComponent properly with this/that.
    */
    public uploadCareSetup(): void {
        const that = this;
        setTimeout(() => {
            that.uploadWidget = uploadcare.Widget('#file');
            if (that.uploadWidget) {
                that.uploadWidget
                .onUploadComplete((fileInfo: any) => {
                    var image = new Image;
                    image.url = fileInfo.cdnUrl;
                    this.campaign.images.push(image);
                    this.patch(function() {
                        window.location.reload();
                    });
                });
            }
        }, 1500);
    }

    /**
    * Delete the campaign image.
    */
    public onDeleteImage(image: Image): void {
        this.unassociateImage(image);
    }

    /**
    * Get the campaign.
    */
    public load(): void {
        this.route.params
        .switchMap((params: Params) => {
            let relationships = 'images,links,rewards,user';
            return params.id > 0 ? this.campaignService.one(params.id, relationships) : Observable.empty();
        })
        .subscribe(
            (response: Campaign) => {
                this.campaign = response;
                if (this.stepIndex == 1) {
                    this.uploadCareSetup();
                }
                this.campaign.links = this.campaign.links ? this.campaign.links : [];
                this.campaign.rewards = this.campaign.rewards ? this.campaign.rewards : [];
            },
            (error: any) => {
                this.notify.error('Could not load this campaign.');
            },
        );
    }

    /**
    * Progress step
    */
    public save(): void {
        if (this.method == 'create') {
            this.stepIndex ++;
            this.displayStep ++;
            this.router.navigate(['/campaigns/' + this.campaign.id + '/' + this.method + '/' + this.stepIndex]);
        } else {
            this.load();
        }
    }

    /**
    * Submit Campaign
    */
    public submitCampaign(): void {
        this.store();
    }

    /**
    * Use the CampaignService to post/patch the campaign.
    */
    public store(): void {
        this.campaignService
        .store(this.campaign)
        .subscribe(
            (response: any) => {
                if (response.data.id > 0) {
                    this.campaign = response.data;
                    this.save();
                }
                this.notify.success(response.message);
            },
            (error: any) => {
                this.notify.error('Error saving.', error.message || 'Could not save this campaign.');
            },
        );
    }

    public fileEvent($event) {
        const fileSelected: File = $event.target.files[0];
        this.campaign.images[0].files = fileSelected;
    }

    /**
    * Use the CampaignService to store and assign images and media to the campaign
    */
    public storeMedia(): void {

        // set featured image (if none)
        if (!this.campaign.image_id && this.campaign.images.length > 0) {
        console.log('this.campaign.images[0].id = ', this.campaign.images[0].id);
            this.campaign.image_id = this.campaign.images[0].id;
        }

        // set featured video (if none)
        if (!this.campaign.video_id && this.campaign.links) {
            this.campaign.links.forEach((link, index) => {
                if (link.key == 'youtube') {
                    this.campaign.video_id = link.id;
                }
            });
        }

        this.patch(function() {
            this.save();
        }.bind(this));
    }

    /**
    * Update the CampaignService
    */
    public patch(callback?): void {
        this.campaignService
        .patch(this.campaign)
        .subscribe(
            (response: any) => {
                this.notify.success(response.message);
                if (callback) {
                    callback();
                }
            },
            (error: any) => {
                this.notify.error('Error saving.', error.message || 'Could not save this campaign.');
            },
        );
    }

    /**
    * Add link form
    */
    public addLinkForm(): void {
        var link = new Link;
        var linkType = this.linkTypes[this.linkTypeIndex];
        link.key = linkType.key;
        link.placeholder = linkType.placeholder;
        link.name = linkType.name;
        this.campaign.links.push(link);
    }

    /**
    * Add link form
    */
    public addRewardForm(): void {
        this.campaign.rewards.push(new Reward);
    }

    /**
    * Submit Fundraising Goals
    */
    public onSubmitFundraisingGoals(): void {
        if (this.method == 'create') {
            this.save();
        } else {
            this.patch(function() {
                this.save();
            }.bind(this));
        }
    }

    /**
    * Submit Rewards
    */
    public onSubmitRewards(): void {
        if (this.method == 'create') {
            this.patch(function() {
                this.router.navigate(['/campaigns/' + this.campaign.id]);
            }.bind(this));
        } else {
            this.patch(function() {
                this.save();
            }.bind(this));
        }
    }

    /**
    * Associate the image with this campaign.
    */
    public unassociateImage(image: Image): void {
        this.associationService
        .delete('campaigns', this.campaign.id, 'images', image.id)
        .subscribe(
            (response) => {
                this.load();
                this.notify.success('Image unassociated with this Campaign.');
            },
            (error: any) => {
                this.notify.error(error.message || 'Could not remove Image association to this Campaign.');
            },
        );
    }

    /**
    * Unassociate
    */
    public unassociate(relationship: string, object: any, callback?): void {
        this.associationService
        .delete('campaigns', this.campaign.id, relationship, object.id)
        .subscribe(
            (response) => {
                this.load();
                this.notify.success('Deleted.');
                if (callback) {
                    callback();
                }
            },
            (error: any) => {
                this.notify.error(error.message || 'Could not remove object association to this campaign.');
            },
        );
    }

    /**
    * Detach Relation
    */
    public detach(relationship: string, object: any, index: number): void {
        if (object.id) {
            this.unassociate(relationship, object, function() {
                this.campaign[relationship].splice(index, 1);
            }.bind(this));
        } else {
            this.campaign[relationship].splice(index, 1);
        }
    }
}
