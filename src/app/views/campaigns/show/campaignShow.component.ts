import { Component, Inject, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { Location } from '@angular/common';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { NotificationsService } from 'angular2-notifications';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/empty';

import { CampaignService } from '../../../services/campaign.service';
import { CommentService } from '../../../services/comment.service';
import { Campaign } from '../../../models/campaign.model';
import { Comment } from '../../../models/comment.model';
import { TokenService } from '../../../services/token.service';
import { User } from '../../../models/user.model';
import { Environment, FC_ENV } from '../../../models/environment.model';

@Component({
    templateUrl: './campaignShow.component.html',
})
export class CampaignShowComponent implements OnInit {
    public campaign: Campaign = new Campaign;
    public comment: Comment = new Comment;
    public user: User;

    constructor(
        @Inject(FC_ENV) public env: Environment,
        private _sanitizer: DomSanitizer,
        private route: ActivatedRoute,
        private router: Router,
        private location: Location,
        private notify: NotificationsService,
        private campaignService: CampaignService,
        private commentService: CommentService,
        private tokenService: TokenService,
    ) {}

    /**
    * setTimeout() fills me with pain, but the DOM element does not exist yet,
    * even on latest lifecycle ngAfterViewInit().
    */
    public ngOnInit(): void {
        this.load();
        this.user = this.tokenService.getUser();
    }

    /**
    * Get the campaign.
    */
    public load(): void {
        this.route.params
        .switchMap((params: Params) => {
            let relationships = 'comments,comments.user,image,images,links,payments,rewards,user,users,video';
            return params.id > 0 ? this.campaignService.one(params.id, relationships) : Observable.empty();
        })
        .subscribe(
            (response: Campaign) => {
                this.campaign = response;
                this.comment.commentable_id = this.campaign.id;
                this.comment.commentable_type = 'App\\Campaign';

                // sanitize Youtube links
                if (this.campaign.video) {
                    this.campaign.video.safe_url = this._sanitizer.bypassSecurityTrustResourceUrl(this.campaign.video.embed_url);
                }
                this.campaign.links.forEach((link, index) => {
                    if (link.key == 'youtube') {
                        this.campaign.links[index].safe_url = this._sanitizer.bypassSecurityTrustResourceUrl(link.embed_url);
                    }
                });
            },
            (error: any) => {
                this.notify.error('Could not load this campaign.');
            },
        );
    }

    /**
    * Submit Comment
    */
    public submitComment(): void {
        this.postComment(function() {
            this.comment.user = this.user;
            this.campaign.comments.push(this.comment);
            this.comment = new Comment;
        }.bind(this))
    }

    /**
    * Post Comment
    */
    public postComment(callback?): void {
        console.log('this.comment = ', this.comment);
        this.commentService
        .post(this.comment)
        .subscribe(
            (response: any) => {
                this.notify.success("Comment posted.");
                callback();
            },
            (error: any) => {
                this.notify.error('Error saving.', error.message || 'Could not post this comment.');
            },
        );
    }

    /**
    * Toggle Campaign Live Status
    */
    public toggleLive(): void {
        this.patch(function() {
            setTimeout(function() {
                window.location.href = `${this.env.seo_url}/c/${this.campaign.id}`;
            }.bind(this), 3000);
        }.bind(this));
    }

    /**
    * Update the CampaignService
    */
    public patch(callback?): void {
        this.campaignService
        .patch(this.campaign)
        .subscribe(
            (response: any) => {
                this.notify.success(response.message);
                if (callback) {
                    callback();
                }
            },
            (error: any) => {
                this.notify.error('Error saving.', error.message || 'Could not save this campaign.');
            },
        );
    }
}
