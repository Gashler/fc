import { Inject } from '@angular/core';

import { Component, OnInit } from '@angular/core';

import { CampaignService } from '../../services/campaign.service';
import { Campaign, CampaignPaginatedResponse } from '../../models/campaign.model';
import { Environment, FC_ENV } from '../../models/environment.model';


@Component({
  selector: 'si-campaigns',
  templateUrl: './home.component.html',
})
export class HomeComponent implements OnInit {
  public campaigns: Campaign[];
  public page = 1;

  constructor(
      private campaignService: CampaignService,
      @Inject(FC_ENV) public env: Environment,
  ) {}

  public ngOnInit(): void {
    this.load();
  }

  /**
   * Get the campaigns.
   */
  public load(page: number = this.page): void {
    this.page = page || 1;

    this.campaignService
      .active()
      .subscribe((response: CampaignPaginatedResponse) => {
        this.campaigns = response.data;
      });
  }
}
