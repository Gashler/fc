import { Component, OnInit, Injectable, Inject } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { Location } from '@angular/common';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { NotificationsService } from 'angular2-notifications';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/empty';

import { CampaignService } from '../../../services/campaign.service';
import { PaymentService } from '../../../services/payment.service';
import { Campaign } from '../../../models/campaign.model';
import { Environment, FC_ENV } from '../../../models/environment.model';
import { Payment } from '../../../models/payment.model';
import { Reward } from '../../../models/reward.model';
import { TokenService } from '../../../services/token.service';
import { User } from '../../../models/user.model';

// Stripe
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { StripeInstance, StripeFactoryService, Elements, Element as StripeElement, ElementsOptions } from "ngx-stripe";

@Component({
    selector: 'fc-payment',
    templateUrl: './paymentCreate.component.html',
})
export class PaymentCreateComponent implements OnInit {
    public campaign: Campaign;
    public loading: boolean = false;
    public payment: Payment = new Payment();
    public recommendedPaymentAmount = 25;
    public rewardIndex: number;
    public user: User;

    // Stripe
    stripeInstance: StripeInstance;
    elements: Elements;
    card: StripeElement;

    // optional parameters
    elementsOptions: ElementsOptions = {
        locale: 'en'
    };

    stripeTest: FormGroup;

    constructor(
        private _sanitizer: DomSanitizer,
        private route: ActivatedRoute,
        private router: Router,
        private location: Location,
        private notify: NotificationsService,
        private campaignService: CampaignService,
        private paymentService: PaymentService,
        private tokenService: TokenService,

        // Stripe
        private fb: FormBuilder,
        private stripeFactory: StripeFactoryService,
        @Inject(FC_ENV) protected env: Environment,
    ) {
        var rewardIndex = this.route.snapshot.params.reward_index;
        if (rewardIndex) {
            this.rewardIndex = rewardIndex;
            this.rewardIndex ++;
        } else {
            this.rewardIndex = 0;
        }
    }

    /**
    * setTimeout() fills me with pain, but the DOM element does not exist yet,
    * even on latest lifecycle ngAfterViewInit().
    */
    public ngOnInit(): void {
        if (!this.user) {
            this.user = new User;
        }
        this.payment.user = this.user;
        this.load();
        if (this.user = this.tokenService.getUser()) {
            this.payment.user = this.user;
        }
        this.stripeInstance = this.stripeFactory.create(this.env.stripe_key);

        // Stripe
        this.stripeTest = this.fb.group({
            name: ['', [Validators.required]]
        });
    }

    /**
    * Get the campaign.
    */
    public load(): void {
        this.route.params
        .switchMap((params: Params) => {
            let relationships = 'rewards,user';
            return this.campaignService.one(params.campaign_id, relationships);
        })
        .subscribe(
            (response: any) => {
                this.campaign = response;
                this.payment.campaign_id = this.campaign.id;

                // create empty reward
                var reward = new Reward;
                reward.title = 'No Reward';
                this.campaign.rewards.unshift(reward);

                this.setPaymentAmount();

                // Stripe
                this.stripeInstance.elements(this.elementsOptions)
                .subscribe(elements => {
                    this.elements = elements;
                    // Only mount the element the first time
                    if (!this.card) {
                        this.card = this.elements.create('card', {
                            style: {
                                base: {
                                    iconColor: '#666EE8',
                                    color: '#31325F',
                                    lineHeight: '40px',
                                    fontWeight: 300,
                                    fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
                                    fontSize: '18px',
                                    '::placeholder': {
                                        color: '#CFD7E0'
                                    }
                                }
                            }
                        });
                        this.card.mount('#card-element');
                    }
                });
            },
            (error: any) => {
                this.notify.error('Could not load this campaign.');
            },
        );
    }

    /**
    * Submit a payment.
    */
    public onSubmitPayment(): void {
        this.storePayment();
    }

    /**
    * Get Stripe Token
    */
    public getStripeToken() {
        if (this.payment.token) {
            return this.storePayment();
        }
        this.loading = true;
        const name = this.payment.user.first_name + ' ' + this.payment.user.last_name;
        this.stripeInstance
        .createToken(this.card, { name })
        .subscribe(result => {
            if (result.token) {
                this.payment.token = result.token.id;
                this.storePayment();
            } else if (result.error) {
                this.loading = false;
                // console.log('Stripe Error: ', result.error.message);
                this.notify.error('Payment error.', result.error.message || ' Could not make this payment.');
            }
        });
    }

    /**
    * Use the PaymentService to post/patch the payment.
    */
    public storePayment(): void {
        if (this.payment.amount < this.campaign.rewards[this.rewardIndex].amount) {
            this.notify.error(
                'Amount too low.', "In order to qualify for this reward, you must contribute at least $" + this.campaign.rewards[this.rewardIndex].amount + '. Either raise the amount or select a different award.'
            );
            return;
        }
        if (this.tokenService.getUser()) {
            var endpoint = 'payment';
        } else {
            var endpoint = 'payments';
        }

        // set payment's reward ID
        this.payment.reward_id = this.campaign.rewards[this.rewardIndex].id;

        this.paymentService
        .store(this.payment, endpoint)
        .subscribe(
            (response: any) => {
                this.loading = false;
                this.notify.success(response.message);
                this.tokenService.store(response);
                this.router.navigate([`/payments/${response.payment.id}/confirmation`]);
            },
            (error: any) => {
                this.loading = false;
                this.notify.error('Payment error.', error.message || 'Could not make this payment.');
                if (error.key == 'email') {
                    var message = "A user already exists with this email address. Please log in, then try again."
                    if (confirm(message)) {
                        this.router.navigate([`/login`]);
                    }
                }
            },
        );
    }

    public setPaymentAmount()
    {
        this.payment.amount = this.campaign.rewards[this.rewardIndex].amount;
        if (!this.payment.amount || this.payment.amount == 0) {
            this.payment.amount = this.recommendedPaymentAmount;
        }
    }
}
