import { Injectable, Inject } from '@angular/core';
import { Component, OnInit, ViewChild } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

import { Location } from '@angular/common';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { NotificationsService } from 'angular2-notifications';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/empty';

import { PaymentService } from '../../../services/payment.service';
import { Campaign } from '../../../models/campaign.model';
import { Comment } from '../../../models/comment.model';
import { Environment, FC_ENV } from '../../../models/environment.model';
import { Payment } from '../../../models/payment.model';
import { Reward } from '../../../models/reward.model';
import { Subscription } from '../../../models/subscription.model';
import { AssociationService } from '../../../services/association.service';
import { CampaignService } from '../../../services/campaign.service';
import { CommentService } from '../../../services/comment.service';
import { TokenService } from '../../../services/token.service';
import { UserService } from '../../../services/user.service';
import { User } from '../../../models/user.model';

@Component({
    selector: 'si-campaign',
    templateUrl: './paymentConfirmation.component.html',
})
export class PaymentConfirmationComponent implements OnInit {
    public comment: Comment = new Comment();
    public loading: boolean = false;
    public payment: Payment = null;
    public step: number = 1;
    public subscriptions = [
        {
            description: "Keep me updated with the progress of this campaign",
            id: null,
            key: 'campaign',
            subscribed: true
        },
        {
            description: "Keep me updated with other exciting opportunies from FunderCat",
            id: 1,
            key: 'backers',
            subscribed: true
        },
    ];
    public user: User;

    @ViewChild('passwordForm') public form: NgForm;

    constructor(
        private _sanitizer: DomSanitizer,
        private route: ActivatedRoute,
        private router: Router,
        private location: Location,
        private associationService: AssociationService,
        private commentService: CommentService,
        private notify: NotificationsService,
        private campaignService: CampaignService,
        private paymentService: PaymentService,
        private tokenService: TokenService,
        private userService: UserService,
        @Inject(FC_ENV) protected env: Environment,
    ) {}

    /**
    * setTimeout() fills me with pain, but the DOM element does not exist yet,
    * even on latest lifecycle ngAfterViewInit().
    */
    public ngOnInit(): void {

        // get user
        this.user = this.tokenService.getUser();

        // define step
        var step = this.route.snapshot.params.step;
        if (step) {
            this.step = step;
        } else {
            this.step = 1;
            if (this.user.has_password) {
                this.step = 2;
            }
        }

        this.load();
        this.user = this.tokenService.getUser();
    };

    /**
    * Get the payment.
    */
    public load(): void {
        this.route.params
        .switchMap((params: Params) => {
            return this.paymentService.one(params.id);
        })
        .subscribe(
            (response: any) => {
                this.payment = response;
                this.payment.anonymous = false;
                this.comment.commentable_id = this.payment.campaign.id;
                this.comment.commentable_type = 'App\\Campaign';
                this.comment.user_id = this.user.id;

                // get user's subscriptions
                for (let user_subscription of this.user.subscriptions) {
                    for (let key in this.subscriptions) {
                        let value = this.subscriptions[key];
                        if (key == user_subscription.key) {
                            this.subscriptions[key].subscribed = true;
                        }
                    }
                }
            },
            (error: any) => {
                this.notify.error('Could not load this payment.');
            },
        )
    }

    /**
    * Skip password
    */
    public skipPassword(): void {
        var message = "Adding a password will make it easier and faster to support future campaigns. Without a password, you won't be able to view your payment history, leave comments, follow campaigns, or create your own campaign. Are you sure?"
        if (confirm(message)) {
            //
        }
    }

    /**
    * Progress step
    */
    public progressStep(): void {
        this.step ++;
        this.router.navigate(['/payments/' + this.payment.id + '/confirmation/' + this.step]);
    }

    /**
    * Submit Password
    */
    public submitPassword(): void {
        if (this.user.password !== this.user.c_password) {
            this.notify.error('Passwords do not match.');
            return;
        }
        this.patchUser(function() {
            this.progressStep();
        }.bind(this));
    }

    /**
    * Patch payment
    */
    public patchPayment(callback?): void {
        this.paymentService
        .patch(this.payment)
        .subscribe(
            (response: any) => {
                this.notify.success(response.message);
                callback();
            },
            (error: any) => {
                this.notify.error('Error saving.', error.message || 'Could not save this payment.');
            },
        );
    }

    /**
    * Patch user
    */
    public patchUser(callback?): void {
        this.userService
        .patch(this.user)
        .subscribe(
            (response: any) => {
                this.notify.success('Preferences saved.');
                if (callback) {
                    callback();
                }
            },
            (error: any) => {
                this.notify.error('Error saving.', error.message || 'Could not save this user.');
            },
        );
    }

    /**
    * Submit Preferences
    */
    public submitPreferences(callback?): void {
        this.patchUser();
        for (let subscription of this.subscriptions) {
            if (subscription.key == 'campaign') {
                if (subscription.subscribed) {
                    this.associateSubscription(this.payment.campaign.subscription.id);
                } else {
                    this.unassociateSubscription(this.payment.campaign.subscription.id);
                }
            }
            if (subscription.key == 'backers') {
                if (subscription.subscribed) {
                    this.associateSubscription(subscription.id, this.step2Callback.bind(this));
                } else {
                    this.unassociateSubscription(subscription.id, this.step2Callback.bind(this));
                }
            }
        }
    }

    public step2Callback(): void {
        console.log('line 211');
        this.progressStep();
    }

    /**
    * Associate a subscription with this campaign.
    */
    public associateSubscription(subscription_id: number, callback?): void {
        this.associationService
        .post('users', this.user.id, 'subscriptions', subscription_id)
        .subscribe(
            (response) => {
                this.load();
                // this.notify.success('Subscription associated with this user.');
                if (callback) {
                    callback();
                }
            },
            (error: any) => {
                this.notify.error(error.message || 'Could not associate subscription with this user.');
            },
        );
    }

    /**
    * Associate a subscription with this campaign.
    */
    public unassociateSubscription(subscription_id: number, callback?): void {
        this.associationService
        .delete('users', this.user.id, 'subscriptions', subscription_id)
        .subscribe(
            (response) => {
                this.load();
                // this.notify.success('Subscription unassociated with this user.');
                if (callback) {
                    callback();
                }
            },
            (error: any) => {
                this.notify.error(error.message || 'Could not unassociate subscription with this user.');
            },
        );
    }

    /**
    * Submit Comment
    */
    public submitComment(): void {
        console.log('SubmitComment!');
        this.postComment(function() {
            this.progressStep();
        }.bind(this));
    }

    /**
    * Post Comment
    */
    public postComment(callback?): void {
        console.log('this.comment = ', this.comment);
        this.commentService
        .post(this.comment)
        .subscribe(
            (response: any) => {
                this.notify.success(response.message);
                callback();
            },
            (error: any) => {
                this.notify.error('Error saving.', error.message || 'Could not post this comment.');
            },
        );
    }
}
