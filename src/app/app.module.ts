import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule, Injectable } from '@angular/core';
import { NgxStripeModule } from 'ngx-stripe';
import { CommonModule } from '@angular/common';
import { BrowserXhr, HttpModule, Http } from '@angular/http';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { Router } from '@angular/router';
import { SimpleNotificationsModule } from 'angular2-notifications';
import { NgProgressModule, NgProgressBrowserXhr } from 'ngx-progressbar';
import { HashLocationStrategy, LocationStrategy } from '@angular/common';

import { FC_ENV } from '../app/models/environment.model';
import { environment } from '../environments/environment';

import { User } from '../app/models/user.model';
// import {ShareButtonsModule} from 'ngx-sharebuttons';

// App-level Components
import { AppComponent } from './app.component';
import { LoginComponent } from './views/login';
import { NotFoundComponent } from './views/not-found';
import { LayoutComponent } from './layout';
import {
  AppAside,
  AppFooter,
  AppHeader,
  AppSidebar,
  AppSidebarFooter,
  AppSidebarForm,
  AppSidebarHeader,
} from './components';

const APP_COMPONENTS = [
  AppAside,
  AppFooter,
  AppHeader,
  AppSidebar,
  AppSidebarFooter,
  AppSidebarForm,
  AppSidebarHeader,
  LoginComponent,
  NotFoundComponent,
  LayoutComponent,
];


// App-level Directives
import {
  AsideToggleDirective,
  NAV_DROPDOWN_DIRECTIVES,
  SIDEBAR_TOGGLE_DIRECTIVES,
} from './directives';

const APP_DIRECTIVES = [
  AsideToggleDirective,
  NAV_DROPDOWN_DIRECTIVES,
  SIDEBAR_TOGGLE_DIRECTIVES,
];


// App-level Services
import { AuthGuard } from './guards/auth-guard.service';
import { RoleGuard } from './guards/role-guard.service';
import { TokenService } from './services/token.service';
import { HttpService } from './services/http.service';
import { LoginService } from './services/login.service';


// Routing module
import { AppRoutingModule } from './app.routing';

// 3rd party components
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { TabsModule } from 'ngx-bootstrap/tabs';


@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    CommonModule,
    FormsModule, ReactiveFormsModule,
    HttpModule,
    AppRoutingModule,
    NgProgressModule,
    NgxStripeModule.forRoot(),
    BsDropdownModule.forRoot(),
    TabsModule.forRoot(),
    // ShareButtonsModule.forRoot(),
    SimpleNotificationsModule.forRoot(),
  ],
  declarations: [
    AppComponent,
    ...APP_COMPONENTS,
    ...APP_DIRECTIVES,
  ],
  providers: [
    { provide: FC_ENV, useValue: environment },
    {provide: LocationStrategy, useClass: HashLocationStrategy},
    {
      provide: BrowserXhr,
      useClass: NgProgressBrowserXhr,
    },
    TokenService,
    {
      provide:  User,
      useClass: User,
      deps:     [Router, TokenService],
    },
    {
      provide:  AuthGuard,
      useClass: AuthGuard,
      deps:     [Router, TokenService],
    },
    {
      provide:  RoleGuard,
      useClass: RoleGuard,
      deps:     [Router, TokenService],
    },
    {
      provide:  HttpService,
      useClass: HttpService,
      deps:     [Http, TokenService],
    },
    {
      provide:  LoginService,
      useClass: LoginService,
      deps:     [HttpService, FC_ENV],
    },
  ],
  bootstrap: [ AppComponent ]
})
export class AppModule {}
