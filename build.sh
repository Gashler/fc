# Update Backend
# cd /home/html/fc-backend
# git pull
# cp .env.prod .env
# php artisan migrate

# Update Frontend
cd /home/html/fc-frontend
git pull;
cp src/environments/environment.prod.ts src/environments/environment.ts
yarn build --prod --build-optimizer --output-path=dist-new --aot;
rm -rf dist-old
mv dist dist-old
mv dist-new dist
